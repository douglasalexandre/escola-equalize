

/**
 * Created by bluesprogrammer on 26/07/15.
 */

var app = angular.module('equalizejs', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
]);

app.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/list.html',
        controller: 'ListCtrl'
    }).when('/novo', {
        templateUrl: 'views/novo.html',
        controller: 'CreateCtrl'
    }).otherwise({
        redirectTo: '/'
    })
});

app.controller('ListCtrl', function ($scope, $http) {
    $http.get('/api/v1/alunos').success(function (data) {
        $scope.alunos = data;
    }).error(function (data, status) {
        console.log('Error ' + data)
    })
});

app.controller('CreateCtrl', function ($scope, $http, $location) {
    
    $scope.createAluno = function () {
        console.log($scope.aluno);
        $http.post('/api/v1/alunos', $scope.aluno).success(function (data) {
            $location.path('/');
        }).error(function (data, status) {
            console.log('Error ' + data)
        })
    }
});

app.controller('UpdateCtrl', function ($scope, $http, $location){
    $scope.updateAluno = function (){
        console.log($scope.aluno);
        $http.put('alunos/:id', $scope.aluno).success(function (data){
            $location.path('/:id');
        }).error(function (data, status){
            console.log('Error' + data)
        })
    }
});

app.controller('EditCtrl', function(){
    $scope.editAluno = function(id) {
        $scope.aluno.nome = $scope.alunos[id-1].aluno.nome;
        $scope.aluno.cursos = $scope.alunos[id-1].aluno.cursos;
        $scope.aluno.diaDasAulas = $scope.alunos[id-1].aluno.diaDasAulas;
        $scope.aluno.horarios = $scope.alunos[id-1].aluno.horarios;
        $scope.aluno.telefones = $scope.alunos[id-1].aluno.telefones;
        $scope.aluno.email = $scope.alunos[id-1].aluno.email;
        $scope.aluno.cep = $scope.alunos[id-1].aluno.cep;
        $scope.aluno.cpf = $scope.alunos[id-1].aluno.cpf;
        $scope.aluno.pagamento = $scope.alunos[id-1].aluno.pagamento;
        $scope.aluno.criadoEm = $scope.alunos[id-1].aluno.criadoEm;
        $scope.aluno.fimDoCurso = $scope.alunos[id-1].aluno.fimDoCurso;
        $scope.observacao = $scope.alunos[id-1].aluno.observacao;    
    }
});