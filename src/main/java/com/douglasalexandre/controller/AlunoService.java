/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.douglasalexandre.controller;

import com.douglasalexandre.model.Aluno;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;

/**
 *
 * @author bluesprogrammer
 */
public class AlunoService {
    
    private final DB db;
    private final DBCollection collection;

    public AlunoService(DB db) {
        this.db = db;
        this.collection = db.getCollection("escolaEqualize");
    }

    public List<Aluno> findAll() {
        List<Aluno> alunos = new ArrayList<>();
        DBCursor dbObjects = collection.find();
        while (dbObjects.hasNext()) {
            DBObject dbObject = dbObjects.next();
            alunos.add(new Aluno((BasicDBObject) dbObject));
        }
        return alunos;
    }

    public void createNewAluno(String body) {
        Aluno aluno = new Gson().fromJson(body, Aluno.class);
        collection.insert(new BasicDBObject("nome", aluno.getNome())
                .append("cursos", aluno.getCursos())
                .append("telefones", aluno.getTelefones())
                .append("email", aluno.getEmail())
                .append("cep", aluno.getCep())
                .append("cpf", aluno.getCpf())
                .append("pagamento", aluno.getPagamento())
                .append("fimDoCurso", aluno.getFimDoCurso())
                .append("observacao", aluno.getObservacao())
                .append("versaoDB", aluno.getVersaoDB())
                .append("criadoEm", new Date()));
    }

    public Aluno find(String id) {
        return new Aluno((BasicDBObject) collection.findOne(new BasicDBObject("_id", new ObjectId(id))));
    }

    public Aluno updateAluno(String alunoId, String body) {
        Aluno aluno = new Gson().fromJson(body, Aluno.class);
        collection.update(new BasicDBObject("_id", new ObjectId(alunoId)), new BasicDBObject("$set", new BasicDBObject("criadoEm", aluno.getCriadoEm())));
        return this.find(alunoId);
    }
    
}
