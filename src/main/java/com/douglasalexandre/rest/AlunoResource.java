/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.douglasalexandre.rest;

import com.douglasalexandre.controller.AlunoService;
import com.douglasalexandre.controller.JsonTransformer;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

/**
 *
 * @author bluesprogrammer
 */
public class AlunoResource {
    
     private static final String API_CONTEXT = "/api/v1";

    private final AlunoService alunoService;

    public AlunoResource(AlunoService alunoService) {
        this.alunoService = alunoService;
        setupEndpoints();
    }

    private void setupEndpoints() {
        post(API_CONTEXT + "/alunos", "application/json", (request, response) -> {
            alunoService.createNewAluno(request.body());
            response.status(201);
            return response;
        }, new JsonTransformer());

        get(API_CONTEXT + "/alunos/:id", "application/json", (request, response)

                -> alunoService.find(request.params(":id")), new JsonTransformer());

        get(API_CONTEXT + "/alunos", "application/json", (request, response)

                -> alunoService.findAll(), new JsonTransformer());

        put(API_CONTEXT + "/alunos/:id", "application/json", (request, response)

                -> alunoService.updateAluno(request.params(":id"), request.body()), new JsonTransformer());
    }
    
}
