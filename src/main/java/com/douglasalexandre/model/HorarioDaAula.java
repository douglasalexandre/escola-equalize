/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.douglasalexandre.model;

/**
 *
 * @author bluesprogrammer
 */
public class HorarioDaAula {
    
    private String hora;

    public HorarioDaAula(String hora) {
        this.hora = hora;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
    
    
    
}
