/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.douglasalexandre.model;

import com.mongodb.BasicDBObject;
import java.util.Date;
import org.bson.types.ObjectId;

/**
 *
 * @author bluesprogrammer
 */
public class Aluno {
    
    private String id;
    
    private String nome;
    
    private String cursos;
    
    private String diaDasAulas;
    
    private String horarios;
    
    private String telefones;
    
    private String email;

    private String cep;
    
    private String cpf;
    
    //a vista, cartao, boleto
    private String pagamento;
    
    private String fimDoCurso;
    
    private String observacao;
    
    private int versaoDB;
    
    private Date criadoEm = new Date();

    public Aluno(BasicDBObject dbObject) {
        this.id = ((ObjectId) dbObject.get("_id")).toString();
        this.nome = dbObject.getString("nome");
        this.cursos = dbObject.getString("cursos");
        this.diaDasAulas = dbObject.getString("diaDasAulas");
        this.horarios = dbObject.getString("horarios");
        this.telefones = dbObject.getString("telefones");
        this.email = dbObject.getString("email");
        this.cep = dbObject.getString("cep");
        this.cpf = dbObject.getString("cpf");
        this.pagamento = dbObject.getString("pagamento");
        this.fimDoCurso = dbObject.getString("fimDoCurso");
        this.observacao = dbObject.getString("observacao");
        this.versaoDB = dbObject.getInt("versaoDB");
        this.criadoEm = dbObject.getDate("criadoEm");
    }

    public String getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
    
    public String getCursos() {
        return cursos;
    }

    public String getDiaDasAulas() {
        return diaDasAulas;
    }

    public String getHorarios() {
        return horarios;
    }
    
    public String getTelefones() {
        return telefones;
    }

    public String getEmail() {
        return email;
    }
    
    public String getCep() {
        return cep;
    }
    
    public String getCpf() {
        return cpf;
    }

    public String getPagamento() {
        return pagamento;
    }

    public String getFimDoCurso() {
        return fimDoCurso;
    }
    public String getObservacao() {
        return observacao;
    }

    public int getVersaoDB() {
        return versaoDB;
    }

    public Date getCriadoEm() {
        return criadoEm;
    }
    
}
